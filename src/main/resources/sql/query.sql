CREATE TABLE `users` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `user_roles` (
  `user_role_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uni_username_role` (`role`,`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

INSERT INTO user_roles (user_role_id, username, role) VALUES (4, 'admin', 'ROLE_ADMIN');
INSERT INTO user_roles (user_role_id, username, role) VALUES (2, 'mkyong', 'ROLE_ADMIN');
INSERT INTO user_roles (user_role_id, username, role) VALUES (3, 'alex', 'ROLE_USER');
INSERT INTO user_roles (user_role_id, username, role) VALUES (1, 'mkyong', 'ROLE_USER');
INSERT INTO user_roles (user_role_id, username, role) VALUES (5, 'user', 'ROLE_USER');

INSERT INTO bss.users (username, password, enabled) VALUES ('admin', 'admin', 1);
INSERT INTO bss.users (username, password, enabled) VALUES ('alex', '123456', 1);
INSERT INTO bss.users (username, password, enabled) VALUES ('mkyong', '123456', 1);
INSERT INTO bss.users (username, password, enabled) VALUES ('user', 'user', 1);


CREATE TABLE `persistent_logins` (
  `username` varchar(64) NOT NULL,
  `series` varchar(64) NOT NULL,
  `token` varchar(64) NOT NULL,
  `last_used` timestamp NOT NULL,
  PRIMARY KEY (`series`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
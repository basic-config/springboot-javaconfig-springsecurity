package net.suby.project.login.service;

import net.suby.project.login.vo.Users;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-28.
 */
public interface LoginService {
    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
    public Users getUser(String userName);
}

package net.suby.project.login.service.impl;

import net.suby.project.login.dao.jpa.UserJpaRepository;
import net.suby.project.login.dao.mybatis.UserMybatisRepository;
import net.suby.project.login.service.LoginService;
import net.suby.project.login.vo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-28.
 */
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private UserMybatisRepository userMybatisRepository;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Override
    public HashMap<String, String> findUserId(HashMap<String, String> parameter) {
        return userMybatisRepository.findUserId(parameter);
    }

    @Override
    public Users getUser(String userName) {
        return userJpaRepository.getOne(userName);
    }
}

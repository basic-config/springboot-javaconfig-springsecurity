package net.suby.project.login.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by ohs on 2016-12-29.
 */
public interface CustomUserService extends UserDetailsService {
}

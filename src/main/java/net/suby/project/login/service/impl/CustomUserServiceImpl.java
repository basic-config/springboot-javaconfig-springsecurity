package net.suby.project.login.service.impl;

import net.suby.project.login.dao.jpa.UserJpaRepository;
import net.suby.project.login.dao.jpa.UserRoleJpaRepository;
import net.suby.project.login.service.CustomUserService;
import net.suby.project.login.vo.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by ohs on 2016-12-29.
 */
@Service("customUserService")
public class CustomUserServiceImpl implements CustomUserService {
    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private UserRoleJpaRepository userRoleJpaRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users users = userJpaRepository.findOne(username);
        List<String> userRoles = userRoleJpaRepository.findRoleByUserName(username);
        List<GrantedAuthority> authorities = buildUserAuthority(userRoles);
        return buildUserForAuthentication(users, authorities);
    }

    private User buildUserForAuthentication(Users users, List<GrantedAuthority> authorities) {
        return new User(users.getUsername(),users.getPassword(), true, true, true, true, authorities);
    }

    // 권한 설정
    private List<GrantedAuthority> buildUserAuthority(List<String> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<GrantedAuthority>();

        // Build user's authorities
        for (String userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole));
        }

        List<GrantedAuthority> Result = new ArrayList<GrantedAuthority>(setAuths);

        return Result;
    }
}

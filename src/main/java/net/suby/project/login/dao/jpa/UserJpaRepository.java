package net.suby.project.login.dao.jpa;

import net.suby.project.login.vo.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ohs on 2016-12-28.
 */
@Repository
public interface UserJpaRepository extends JpaRepository<Users, String> {
}

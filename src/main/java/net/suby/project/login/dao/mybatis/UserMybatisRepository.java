package net.suby.project.login.dao.mybatis;

import java.util.HashMap;

/**
 * Created by ohs on 2016-12-28.
 */
public interface UserMybatisRepository {
    public HashMap<String, String> findUserId(HashMap<String, String> parameter);
}

package net.suby.project.login.dao.jpa;

import net.suby.project.login.vo.UsersRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by ohs on 2016-12-28.
 */
@Repository
public interface UserRoleJpaRepository extends JpaRepository<UsersRoles, Integer> {

    @Query("select a.role from UsersRoles a, Users b where b.username=?1 and a.username=b.username")
    public List<String> findRoleByUserName(String username);
}

package net.suby.project.config.persistence;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="net.suby.project.**.dao.mybatis", sqlSessionFactoryRef="myBatisSqlSessionFactory")
@EnableTransactionManagement
public class PersistenceMybatisConfig {

    @Bean(name = "myBatisDataSource")
    @ConfigurationProperties(prefix = "spring.mybatis.datasource")
    public DataSource myBatisDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "myBatisSqlSessionFactory")
    public SqlSessionFactory myBatisSqlSessionFactory(@Qualifier("myBatisDataSource") DataSource myBatisDataSource, ApplicationContext applicationContext) throws Exception {
        SqlSessionFactoryBean sqlSessionFactoryBean = new SqlSessionFactoryBean();
        sqlSessionFactoryBean.setDataSource(myBatisDataSource);
        sqlSessionFactoryBean.setMapperLocations(applicationContext.getResources("classpath:mapper/user/*.xml"));
        return sqlSessionFactoryBean.getObject();
    }

    @Bean(name = "myBatisSqlSessionTemplate")
    public SqlSessionTemplate myBatisSqlSessionTemplate(SqlSessionFactory myBatisSqlSessionFactory) throws Exception {

        return new SqlSessionTemplate(myBatisSqlSessionFactory);
    }
}

package net.suby.project.config.security;

import net.suby.project.config.persistence.PersistenceJpaConfig;
import net.suby.project.login.service.CustomUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = {CustomUserService.class, PersistenceJpaConfig.class})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	@Qualifier("customUserService")
	private UserDetailsService userDetailsService;

	@Autowired
	@Qualifier("jpaDataSource")
	private DataSource dataSource;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService);	//.passwordEncoder(passwordEncoder());
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		 http.authorizeRequests()
			.antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
			.antMatchers("/user/**").access("hasRole('ROLE_USER')")
			.antMatchers("/index/**").permitAll()
			.antMatchers("/").permitAll()
			.and()
			    .formLogin().loginPage("/login").failureUrl("/login?error")
			    .defaultSuccessUrl("/index")
			    .usernameParameter("username").passwordParameter("password")
			.and()
		 		.rememberMe()
				 // persistent방식(S) - DB에 저장이 필요하기 때문에 persistent_logins테이블이 필요 - 쿠키방식보다 안전
				.tokenRepository(persistentTokenRepository())
				.tokenValiditySeconds(1209600)
				 // persistent방식(S)

				 // 쿠키방식(S)
//				.key("rem-me-key")
//				.rememberMeParameter("remember-me")
//				.rememberMeCookieName("my-remember-me")
//				.tokenValiditySeconds(86400)
				 // 쿠키방식(E)
			.and()
			    .logout().logoutSuccessUrl("/login?logout")
			.and()
			    .csrf();		// 크로스도메인 체크를 위해서 모든 form에 token 정보를 넣어줘야 한다.
	}

	@Bean
	public PersistentTokenRepository persistentTokenRepository() {
		JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
		db.setDataSource(dataSource);
		return db;
	}

	@Bean
	public PasswordEncoder passwordEncoder(){
		PasswordEncoder encoder = new BCryptPasswordEncoder();
		return encoder;
	}

}

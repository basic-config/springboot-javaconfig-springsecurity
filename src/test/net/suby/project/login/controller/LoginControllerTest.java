package net.suby.project.login.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import net.suby.project.WebApplication;
import net.suby.project.login.service.LoginService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

//import static org.hamcrest.Matchers.*;
//import static org.mockito.BDDMockito.given;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

/**
 * Created by ohs on 2017-01-02.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=WebApplication.class)       // javaconfig 파일을 통한 환경 설정
@AutoConfigureMockMvc                               // MockMvc 설정
public class LoginControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean   // loginService에 가짜 Bean을 등록
    private LoginService loginService;

    @Test
    public void hello() throws Exception {
        this.mockMvc.perform(get("/hello").accept(MediaType.TEXT_PLAIN)) // /hello 라는 url로 text/plain 타입을 요청
                .andExpect(status().isOk())                                          // 위 요청에 따라 결과가 status는 200이며
                .andExpect(content().string("hello, world!!"));     // response body에 "hello, world!!" 가 있는지 검증
    }

    @Test
    public void login() throws Exception {
        this.mockMvc.perform(get("/login").accept(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));  // 호출한 view의 이름이 login인지 확인 (확장자는 생략)
    }

}
